# Post $cript
## Écoles sous licence

Typographie : usages, économie, fichiers, licences.  
7 décembre 2022 | Strasbourg  
6 + 7 mars 2023 | Bruxelles  

![Post $cript cover](/images/Banniere_site_hear_1480x1080px.png)

En janvier 2023, les logiciels Adobe arrêteront d’afficher les polices Postscript de type 1 installées sur le système.* Ce retrait pose une série de questions pour les écoles d’art et leurs utilisateur·ices qui ont fait un investissement dans des polices considérées comme des grands classiques de la typographie au cours des années 90. Aussi confidentiel et spécifique soit-il, ce basculement de format typographique, point de départ de notre réflexion, nous apparaît symptomatique de bouleversements rapides et souvent trop discrets qui s’opèrent au sein de nos outils et de nos institutions. La phase inédite que nous vivons depuis ces dernières décennies offre une opportunité pour rassembler une série de questions lancinantes liées à la typographie mais qui traversent et concernent d’autres disciplines.

*Ce format de fontes a été au cœur de la stratégie industrielle et commerciale de la firme depuis 1984 jusqu’après l’introduction du format Opentype en 1996.

Comment conserver, renouveler l’accès à un patrimoine – typographique – du XXe siècle et d’avant et ainsi, se forger une culture? Comment déjouer les stratégies visant à faire croire qu’il faut payer pour être créatif·ve ? Comment le web, les licences libres et les fontes variables ont twisté les casses et quels effets esthétiques en résultent ? Quelles questions politiques, économiques et impérialistes sourdent sous les choix et les monopoles industriels ? Quelles pédagogies peuvent y répondre et quels investissements peuvent faire sens pour les écoles d’art et les étudianx ?

Deux rencontres aller-retour entre Strasbourg-Bruxelles, organisées par l’atelier de Communication graphique de la HEAR Strasbourg et les départements typographiques de l’erg et de l’ENSAV La Cambre (Bruxelles) avec une série d’invitæs internationaux qui échangeront autour de ces questions afin de trouver des éléments de réponses et des outils possibles à destination des étudianx et des pédagogies.

 

## Programme :

— HEAR, Strasbourg, mercredi 7 décembre 2022 
HEAR, site d’Arts plastiques, Auditorium  
1 rue de l’Académie à Strasbourg   
À partir de 14 h avec :

Frank Adebiaye • About Licencing  
Bye Bye Binary*• Typographie inclusive  
Naïma Ben Ayed • Multiscript dimensions  
Frank Griesshammer • Technological transitions 

Entrée libre dans la limite des places / en ligne (lien à venir)  
Les conférences/échanges se dérouleront en anglais et français

*représentée par Laure Giletti, Pierre Huyghebaert, Ludi Loiseau  

— Bruxelles, 6 + 7 Mars 2023  
Informations à venir

